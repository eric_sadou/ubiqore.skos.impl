# ubiqore skos impl #

[SKOS](http://www.w3.org/TR/2009/REC-skos-reference-20090818/) implementation Data Model in Java.

* Current Version : 1.1.1-RELEASE.

* Maven project.

* this implementation is published under the GNU GENERAL PUBLIC LICENSE Version 3.