/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skosxl.impl;

import org.openrdf.model.Namespace;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.NamespaceImpl;
import org.openrdf.model.impl.ValueFactoryImpl;

public class SKOSXL {

	 public static final java.lang.String NAMESPACE = "http://www.w3.org/2008/05/skos-xl#";
	 // Field descriptor #76 Ljava/lang/String;
	 public static final java.lang.String PREFIX = "skosxl";
	 public static final Namespace NS = new NamespaceImpl(PREFIX, NAMESPACE);
	 public static final URI LABEL;
	 public static final URI LITERAL_FORM;
	 public static final URI PREFLABEL;
	 public static final URI ALTLABEL;
	 public static final URI HIDDENLABEL;
	 public static final URI LABEL_RELATION;
	 static {
			final ValueFactory f = ValueFactoryImpl.getInstance();

			LABEL = f.createURI(NAMESPACE, "Label");
			LITERAL_FORM = f.createURI(NAMESPACE, "literalForm");
			PREFLABEL = f.createURI(NAMESPACE, "prefLabel");
			ALTLABEL = f.createURI(NAMESPACE, "altLabel");
			HIDDENLABEL = f.createURI(NAMESPACE, "hiddenLabel");
			LABEL_RELATION = f.createURI(NAMESPACE, "labelRelation");
		
	 }
}
