/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skosxl.impl;

import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;

/*
 * skosxl:Label
 * S47	skosxl:Label is an instance of owl:Class. [we don't really care about that]
   S48	skosxl:Label is disjoint with each of skos:Concept, skos:ConceptScheme and skos:Collection. [ok]
   S49	skosxl:literalForm is an instance of owl:DatatypeProperty. [OK]
   S50	The rdfs:domain of skosxl:literalForm is the class skosxl:Label. [OK]
   S51	The rdfs:range of skosxl:literalForm is the class of RDF plain literals. [OK]
   S52	skosxl:Label is a sub-class of a restriction on skosxl:literalForm cardinality exactly 1. [OK]
   
   Other rules : 
   The membership of an instance of skosxl:Label within a SKOS concept scheme can be asserted using the skos:inScheme property. TODO
   B.3.4.1. Dumbing-Down to SKOS Lexical Labels  recuperer le skos:prefLabel from a A skosxl:prefLabel B literalForm 
 */
public class Label {
	URI uri;
	
	PlainLiteral literalForm;
	Set<Label> labelRelation;
	URI context; // used for example as the context of a labelRelation , when this label is the object of the relation.
	public Label(URI uri){
		this.uri=uri;
	}
	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}
	public Literal getLiteralForm() {
		return literalForm;
	}
	public void setLiteralForm(PlainLiteral literalForm) {
		this.literalForm = literalForm;
	}
	public Set<Label> getLabelRelation() {
		return labelRelation;
	}
	public void setLabelRelation(Set<Label> labelRelation) {
		this.labelRelation = labelRelation;
	}
	public URI getContext() {
		return context;
	}
	public void setContext(URI context) {
		this.context = context;
	}
}
