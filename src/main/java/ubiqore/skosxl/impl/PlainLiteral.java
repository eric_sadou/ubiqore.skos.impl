/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skosxl.impl;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.datatype.XMLGregorianCalendar;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;

import ubiqore.skos.impl.lang.Lang;

public class PlainLiteral implements Literal {
	
	String label;
	String language="en";
	public PlainLiteral(String label,String language){
		
		this.label=label.trim();
		String tmp=language.trim();
		try {
			if (Lang.CHECK(tmp))this.language=tmp;
		}
		catch(Exception e ){
			this.language="en";
		}
	}
	
public PlainLiteral(String label){
		
		this.label=label.trim();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3724446224671843268L;

	public String stringValue() {
		return this.getLabel();
	}

	public boolean booleanValue() {
		
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		
		return false;
	}

	public byte byteValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

	public XMLGregorianCalendar calendarValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return null;
	}

	public BigDecimal decimalValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return null;
	}

	public double doubleValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

	public float floatValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

	public URI getDatatype() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value & a Language");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return null;
	}

	public String getLabel() {
		
		return this.label;
	}

	public String getLanguage() {
		
		return this.language;
	}

	public int intValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

	public BigInteger integerValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return null;
	}

	public long longValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

	public short shortValue() {
		if (true)
			try {
				throw new SkosXlExcetion("PlainLiteral has always a String Value");
			} catch (SkosXlExcetion e) {
				
				e.printStackTrace();
			}
		return 0;
	}

}
