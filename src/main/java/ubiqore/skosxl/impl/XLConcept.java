/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skosxl.impl;

import java.util.List;
import java.util.Set;

import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.SKOS;

import ubiqore.skos.impl.CollectionEntity;
import ubiqore.skos.impl.ConceptScheme;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class XLConcept extends XLResource implements CollectionEntity {
// http://www.w3.org/TR/2009/REC-skos-reference-20090818/
	/*
	 * Polymorphism Warning :
	 * Our original goal system "SKOSI" provide some support about Entity Polymorphism. Meaning a Concept could be **also**  a ConceptScheme
	 * a Collection even a  OrderedCollection inside a Resource.
	 * So we need to know preview infos. about member List numbers / topConcept Numbers.
	 */
	private boolean isAlsoACollection=false;
	private boolean isAlsoAnOrderedCollection=false;
	private boolean isAlsoAConceptScheme=false;
	
	private int previewTopConceptNumbers=0; // used for Polymorphism (concept could be also a conceptScheme)
	private int previewMemberNumbers=0; // used for Polymorphism (concept could be also a Collection/OrderedCollection)
	public XLConcept(){}
	public XLConcept(URI uri){
		this.uri=uri;
	}
	private String type=SKOS.CONCEPT.getLocalName();
	public String getType() {
		return type;
	}
	private List<ConceptScheme> inScheme=Lists.newArrayList(),
								topConceptOf=Lists.newArrayList();
	
	private Set<XLConcept> broaders=Sets.newHashSet(),
				 		 narrowers=Sets.newHashSet(),
				 		 related=Sets.newHashSet(),
				 		 broaderTransitives=Sets.newHashSet(),
				 		 narrowerTransitives=Sets.newHashSet(),
				 		 closeMatch=Sets.newHashSet(),
				 		 exactMatch=Sets.newHashSet(),
				 		 broadMatch=Sets.newHashSet(),
				 		 narrowMatch=Sets.newHashSet(),
				 		 relatedMatch=Sets.newHashSet();
	
	private Set<URI> memberOf=Sets.newHashSet();
	
	private int childrenPreviewNumber=0;
	private int relatedPreviewNumber=0;
	public int getRelatedPreviewNumber() {
		return relatedPreviewNumber;
	}
	public void setRelatedPreviewNumber(int relatedPreviewNumber) {
		this.relatedPreviewNumber = relatedPreviewNumber;
	}
	// this need to be use when a concept is the object of a skos:related relation.
	// by default, the graph of the relation is the arbitrary  first subject schemeURI.
	private URI relationGraph=null;
	
	
	public void setMemberOf(Set<URI> set){
		this.memberOf=set;
	}
	public Set<URI> getMemberOf(){
		return memberOf;
	}
	/**
	 * @return the inScheme
	 */
	public List<ConceptScheme> getInScheme() {
		return inScheme;
	}
	/**
	 * @param inScheme the inScheme to set
	 */
	public void setInScheme(List<ConceptScheme> inScheme) {
		this.inScheme = inScheme;
	}
	/**
	 * @return the topConceptOf
	 */
	public List<ConceptScheme> getTopConceptOf() {
		return topConceptOf;
	}
	/**
	 * @param topConceptOf the topConceptOf to set
	 */
	public void setTopConceptOf(List<ConceptScheme> topConceptOf) {
		this.topConceptOf = topConceptOf;
	}
	/**
	 * @return the broaders
	 */
	public Set<XLConcept> getBroaders() {
		return broaders;
	}
	/**
	 * @param broaders the broaders to set
	 */
	public void setBroaders(Set<XLConcept> broaders) {
		this.broaders = broaders;
	}
	/**
	 * @return the narrowers
	 */
	public Set<XLConcept> getNarrowers() {
		return narrowers;
	}
	/**
	 * @param narrowers the narrowers to set
	 */
	public void setNarrowers(Set<XLConcept> narrowers) {
		this.narrowers = narrowers;
	}
	/**
	 * @return the related
	 */
	public Set<XLConcept> getRelated() {
		return related;
	}
	/**
	 * @param related the related to set
	 */
	public void setRelated(Set<XLConcept> related) {
		this.related = related;
	}
	/**
	 * @return the broaderTransitives
	 */
	public Set<XLConcept> getBroaderTransitives() {
		return broaderTransitives;
	}
	/**
	 * @param broaderTransitives the broaderTransitives to set
	 */
	public void setBroaderTransitives(Set<XLConcept> broaderTransitives) {
		this.broaderTransitives = broaderTransitives;
	}
	/**
	 * @return the narrowerTransitives
	 */
	public Set<XLConcept> getNarrowerTransitives() {
		return narrowerTransitives;
	}
	/**
	 * @param narrowerTransitives the narrowerTransitives to set
	 */
	public void setNarrowerTransitives(Set<XLConcept> narrowerTransitives) {
		this.narrowerTransitives = narrowerTransitives;
	}
	/**
	 * @return the closeMatch
	 */
	public Set<XLConcept> getCloseMatch() {
		return closeMatch;
	}
	/**
	 * @param closeMatch the closeMatch to set
	 */
	public void setCloseMatch(Set<XLConcept> closeMatch) {
		this.closeMatch = closeMatch;
	}
	/**
	 * @return the exactMatch
	 */
	public Set<XLConcept> getExactMatch() {
		return exactMatch;
	}
	/**
	 * @param exactMatch the exactMatch to set
	 */
	public void setExactMatch(Set<XLConcept> exactMatch) {
		this.exactMatch = exactMatch;
	}
	/**
	 * @return the broadMatch
	 */
	public Set<XLConcept> getBroadMatch() {
		return broadMatch;
	}
	/**
	 * @param broadMatch the broadMatch to set
	 */
	public void setBroadMatch(Set<XLConcept> broadMatch) {
		this.broadMatch = broadMatch;
	}
	/**
	 * @return the narrowMatch
	 */
	public Set<XLConcept> getNarrowMatch() {
		return narrowMatch;
	}
	/**
	 * @param narrowMatch the narrowMatch to set
	 */
	public void setNarrowMatch(Set<XLConcept> narrowMatch) {
		this.narrowMatch = narrowMatch;
	}
	/**
	 * @return the relatedMatch
	 */
	public Set<XLConcept> getRelatedMatch() {
		return relatedMatch;
	}
	/**
	 * @param relatedMatch the relatedMatch to set
	 */
	public void setRelatedMatch(Set<XLConcept> relatedMatch) {
		this.relatedMatch = relatedMatch;
	}
	/**
	 * @return the relationGraph
	 */
	public URI getRelationGraph() {
		/*if (this.relationGraph==null){
			try {return this.getInScheme().get(0).getUri();}catch (Exception e ){return null;}
		}*/
		return relationGraph;
	}
	/**
	 * @param relationGraph the relationGraph to set
	 */
	public void setRelationGraph(URI relationGraph) {
		this.relationGraph = relationGraph;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("uri", uri)
				.add("notations",notations)
				.add("prefLabels",prefLabels)
				.add("altLabels", altLabels)
				.add("hiddenLabels", hiddenLabels)
				.add("notes",annotations)
				.add("inscheme", inScheme)
				.add("topConceptOf",topConceptOf)
				.add("broaders",broaders)
				.add("narrowers",narrowers)
				.add("related", related)
				.add("relationGraph", relationGraph)
				.add("childrenPreviewNumber", this.childrenPreviewNumber)
				.toString();
	}
	
	public int getChildrenPreviewNumber() {
		return childrenPreviewNumber;
	}
	/**
	 * @param childrenPreviewNumber the childrenPreviewNumber to set
	 */
	public void setChildrenPreviewNumber(int childrenPreviewNumber) {
		this.childrenPreviewNumber = childrenPreviewNumber;
	}
	public boolean isAlsoACollection() {
		return isAlsoACollection;
	}
	public void setAlsoACollection(boolean isAlsoACollection) {
		this.isAlsoACollection = isAlsoACollection;
	}
	public boolean isAlsoAnOrderedCollection() {
		return isAlsoAnOrderedCollection;
	}
	public void setAlsoAnOrderedCollection(boolean isAlsoAnOrderedCollection) {
		this.isAlsoAnOrderedCollection = isAlsoAnOrderedCollection;
	}
	public boolean isAlsoAConceptScheme() {
		return isAlsoAConceptScheme;
	}
	public void setAlsoAConceptScheme(boolean isAlsoAConceptScheme) {
		this.isAlsoAConceptScheme = isAlsoAConceptScheme;
	}
	public int getPreviewTopConceptNumbers() {
		return previewTopConceptNumbers;
	}
	public void setPreviewTopConceptNumbers(int previewTopConceptNumbers) {
		this.previewTopConceptNumbers = previewTopConceptNumbers;
	}
	public int getPreviewMemberNumbers() {
		return previewMemberNumbers;
	}
	public void setPreviewMemberNumbers(int previewMemberNumbers) {
		this.previewMemberNumbers = previewMemberNumbers;
	}
	
	
}
