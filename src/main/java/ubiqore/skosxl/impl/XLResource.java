/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skosxl.impl;

import java.util.Set;

import com.google.common.collect.Sets;

import ubiqore.skos.impl.Resource;
import ubiqore.skos.impl.labels.AltLabel;
import ubiqore.skos.impl.labels.HiddenLabel;
import ubiqore.skos.impl.labels.PrefLabel;

public class XLResource extends Resource {

	Set<Label> xlprefLabels=Sets.newHashSet(); 
	Set<Label> xlhiddenLabels=Sets.newHashSet();
	Set<Label> xlaltLabels=Sets.newHashSet();
	
	public Set<Label> getXlprefLabels() {
		return xlprefLabels;
	}
	public void setXlprefLabels(Set<Label> xlprefLabels) {
		this.xlprefLabels = xlprefLabels;
	}
	public Set<Label> getXlhiddenLabels() {
		return xlhiddenLabels;
	}
	public void setXlhiddenLabels(Set<Label> xlhiddenLabels) {
		this.xlhiddenLabels = xlhiddenLabels;
	}
	public Set<Label> getXlaltLabels() {
		return xlaltLabels;
	}
	public void setXlaltLabels(Set<Label> xlaltLabels) {
		this.xlaltLabels = xlaltLabels;
	}
	@Override
	public Set<AltLabel> getAltLabels(){
		
		Set<AltLabel> s=Sets.newHashSet();
		s.addAll(this.altLabels);
		
		for (Label l:this.xlaltLabels){
			s.add(new AltLabel(l.getLiteralForm().getLabel(),l.getLiteralForm().getLanguage()));
		}
		return s;
	}
	
	@Override
	public Set<HiddenLabel> getHiddenLabels(){
		
		Set<HiddenLabel> s=Sets.newHashSet();
		s.addAll(this.hiddenLabels);
		for (Label l:this.xlhiddenLabels){
			s.add(new HiddenLabel(l.getLiteralForm().getLabel(),l.getLiteralForm().getLanguage()));
		}
		return s;
	}
	
	@Override
	public Set<PrefLabel> getPrefLabels(){
		
		Set<PrefLabel> s=Sets.newHashSet();
		s.addAll(this.prefLabels);
		
		for (Label l:this.xlprefLabels){
			s.add(new PrefLabel(l.getLiteralForm().getLabel(),l.getLiteralForm().getLanguage()));
		}
		return s;
	}
}
