/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;


import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.datatypes.XMLDatatypeUtil;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.model.vocabulary.XMLSchema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
//  ref: http://www.w3.org/TR/skos-reference/#notations
public class Notation  implements StringInfo {

	private String type=SKOS.NOTATION.getLocalName();
	public String getType() {
		return type;
	}
	public Notation (String value){
		this.value=value;
	}
	
	public Notation (Literal lit){
		this.value=lit.getLabel();
		if (lit.getDatatype()!=null)
		this.datatype=lit.getDatatype();
	}
	public Notation (String value, URI datatype){
		this.value=value;
		this.datatype=datatype;
	}
	
	private String value; // 
	private URI datatype=XMLSchema.STRING; // default DataType.
	
	public String getValue() {
		return value;
	}
	
	@JsonSerialize(as=URI.class)
	public URI getDatatype() {
		return datatype;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setDatatype(URI datatype) {
		this.datatype = datatype;
	}
	
	@JsonIgnore
	public Value getModelValue(){
		return new LiteralImpl(this.value,this.datatype);
	}
	@JsonIgnore
	public boolean isPrimitiveDatatype(){
		return XMLDatatypeUtil.isPrimitiveDatatype(this.datatype);
	}
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("value", value)
				.add("datatype", datatype)
				.toString();
	}
	/*@Override
	public boolean equals(Object o) {
		    if (o instanceof Notation) {
		      Notation other = (Notation) o;
		      System.out.println("notation"+other.getDatatype().toString()+this.getDatatype().toString());
		      return (getDatatype().equals(other.getDatatype()) && getValue().equals(other.getValue()));
		    }
		    return false;
	}*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((datatype == null) ? 0 : datatype.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notation other = (Notation) obj;
		if (datatype == null) {
			if (other.datatype != null)
				return false;
		} else if (!datatype.equals(other.datatype))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
		
	
}
