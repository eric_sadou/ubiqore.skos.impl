/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;



import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.datatypes.XMLDatatypeUtil;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.model.vocabulary.XMLSchema;
import org.semarglproject.vocab.XSD;

import ubiqore.skos.impl.lang.Lang;
import ubiqore.skos.impl.notes.ChangeNote;
import ubiqore.skos.impl.notes.Definition;
import ubiqore.skos.impl.notes.EditorialNote;
import ubiqore.skos.impl.notes.Example;
import ubiqore.skos.impl.notes.HistoryNote;
import ubiqore.skos.impl.notes.ScopeNote;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;


public class Note implements StringInfo {
    URI uri=null;
    URI context=null;
    String language="en";
    
    @JsonIgnore
    boolean isALexical=false;
    @JsonIgnore
    boolean isATypedLiteral=false;
    @JsonIgnore
    boolean isAnUriNote=false;
    
	
    private String value; // 
	private URI datatype=XMLSchema.STRING; // default DataType.
    private String type=SKOS.NOTE.getLocalName();
	
    public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
   
   
   
	
	public static Note copy(Note n,URI skosType){
		
		URI    u1=null;
		String v1=null;
		URI d1=null;
		URI c1=null;
		String l1=null;
		if (n.getUri()!=null){
			u1=n.getUri();
			
		}
		else if (n.getLanguage()!=null) {
			l1=n.getLanguage();
			v1=n.getValue();
		}
		else if (n.getDatatype()!=null) {
			d1=n.getDatatype();
			v1=n.getValue();
		}
		
		if (n.getContext()!=null){
			c1=n.getContext();
		}
		
		if (skosType.equals(SKOS.NOTE)){
			if (u1!=null)return new Note(u1,c1);
			else if (l1!=null)return new Note(v1,l1,c1);
			else if (d1!=null)return new Note(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.DEFINITION)){
			if (u1!=null)return new Definition(u1,c1);
			else if (l1!=null)return new Definition(v1,l1,c1);
			else if (d1!=null)return new Definition(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.SCOPE_NOTE)){
			if (u1!=null)return new ScopeNote(u1,c1);
			else if (l1!=null)return new ScopeNote(v1,l1,c1);
			else if (d1!=null)return new ScopeNote(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.EDITORIAL_NOTE)){
			if (u1!=null)return new EditorialNote(u1,c1);
			else if (l1!=null)return new EditorialNote(v1,l1,c1);
			else if (d1!=null)return new EditorialNote(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.HISTORY_NOTE)){
			if (u1!=null)return new HistoryNote(u1,c1);
			else if (l1!=null)return new HistoryNote(v1,l1,c1);
			else if (d1!=null)return new HistoryNote(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.CHANGE_NOTE)){
			if (u1!=null)return new ChangeNote(u1,c1);
			else if (l1!=null)return new ChangeNote(v1,l1,c1);
			else if (d1!=null)return new ChangeNote(v1,d1,c1);
			
		}else if (skosType.equals(SKOS.EXAMPLE)){
			if (u1!=null)return new Example(u1,c1);
			else if (l1!=null)return new Example(v1,l1,c1);
			else if (d1!=null)return new Example(v1,d1,c1);
			
		}
		return null;
	}
    
	

	public Note (URI uri){
		// define a not from a resource URI.
		this.uri=uri;
		this.datatype=null;
		//this.datatype=new URIImpl(XSD.ANY_URI); // pas de DATATYPE pour une URI !!!!! lors d'une definition...
		this.language=null;
		this.isAnUriNote=true;
	}
	
	public Note (URI uri,URI context){
		// define a not from a resource URI.
		this.uri=uri;
		this.context=context;
		this.datatype=null;
		this.language=null;
		this.isAnUriNote=true;
	}
	
	
	
	public Note (String value,URI datatype){
		this.value=value;
		this.language=null;
		if (null!=datatype && !datatype.stringValue().isEmpty()){
			this.datatype=datatype;
		}
		this.isATypedLiteral=true;
		
	}

	public Note (String value,URI datatype ,URI context){
		this.value=value;
		this.language=null;
		if (null!=datatype && !datatype.stringValue().isEmpty()){
			this.datatype=datatype;
		}
		this.isATypedLiteral=true;
		
		this.context=context;
	}

	
	public Note (Literal lit){
		this.value=lit.getLabel();
		
		if (null!=lit.getLanguage()){
			this.isALexical=true;
			this.datatype=null;
			String tmp=lit.getLanguage();
			try {
				if (Lang.CHECK(tmp))this.language=tmp;
			}
			catch(Exception e ){
				this.language="en";
			}
		}else {
			if(lit.getDatatype()!=null)this.datatype=lit.getDatatype();
			this.language=null;
			this.isATypedLiteral=true;
		}
	}
	
	public Note (Literal lit,URI context){
		this.value=lit.getLabel();
		
		if (null!=lit.getLanguage()){
			this.isALexical=true;
			this.datatype=null;
			String tmp=lit.getLanguage();
			try {
				if (Lang.CHECK(tmp))this.language=tmp;
			}
			catch(Exception e ){
				this.language="en";
			}
			
		}else {
			if(lit.getDatatype()!=null)this.datatype=lit.getDatatype();
			this.language=null;
			this.isATypedLiteral=true;
		}
		if (context!=null)this.context=context;
	}
	
	@JsonIgnore
	public Value getModelValue(){
		if (isATypedLiteral)return new LiteralImpl(this.value,this.datatype);
		else if (isALexical)return new LiteralImpl(this.value,this.language); 
		// else if (this.uri!=null && (this.datatype==new URIImpl(XSD.ANY_URI)))return this.getUri();
		else if (isAnUriNote)return this.getUri();
		
		/*else if (this.datatype!=null && this.uri!=null){
			TypedURIImpl t=new TypedURIImpl(this.uri,this.datatype);
			return t;
		 }
		*/
		
		else return null;
		
	}
	
	public Note (String value,String langage){
		this.datatype=null;
		this.value=value;
		if (null!=langage){
			
			try {
				if (Lang.CHECK(langage))this.language=langage;
			}
			catch(Exception e ){
				this.language="en";
			}
		}
		this.isALexical=true;
		
	}
	
	public Note (String value,String langage,URI context){
		this.datatype=null;
		this.value=value;
		if (null!=langage){
			
			try {
				if (Lang.CHECK(langage))this.language=langage;
			}
			catch(Exception e ){
				this.language="en";
			}
		}
		this.isALexical=true;
		this.context=context;
		
	}

	
	public String getValue() {
		return value;
	}
	
	@JsonSerialize(as=URI.class)
	public URI getDatatype() {
		return datatype;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setDatatype(URI datatype) {
		this.datatype = datatype;
	}
	
	@JsonSerialize(as=URI.class)
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	public String getLanguage() {
		return language;
	}
	
	
	public void setLanguage(String language) {
		try {
			if (Lang.CHECK(language))this.language=language;
		}
		catch(Exception e ){
			
		}
	}
	
	
	@Override
	public String toString() {
		ToStringHelper obj= Objects.toStringHelper(this.getClass());
		if (this.isAnUriNote)obj.add("uri",uri);
		else if (this.isALexical)obj.add("value", value).add("language", this.language);
		else if	(this.isATypedLiteral)obj.add("value", value).add("datatype", datatype);
		
		if (this.context!=null)obj.add("context", this.context.toString());
		
		return obj.toString();
	}

	@JsonSerialize(as=URI.class)
	public URI getContext() {
		return context;
	}

	public void setContext(URI context) {
		this.context = context;
	}

	

	public boolean isUri() {
		return isAnUriNote;
	}

	public void setUri(boolean isUri) {
		this.isAnUriNote = isUri;
	}

	
	
}
