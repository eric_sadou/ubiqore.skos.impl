/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl.labels;


import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.vocabulary.SKOS;


import ubiqore.skos.impl.Label;

public class AltLabel extends Label {
	private final String type=SKOS.ALT_LABEL.getLocalName();
	public String getType() {
		return type;
	}
	public AltLabel(String st) {
		super(st);
		// TODO Auto-generated constructor stub
	}
	
	public AltLabel(String st,String language) {
		super(st,language);
		// TODO Auto-generated constructor stub
	}

	public AltLabel(Literal objLit) {
		super(objLit);
	}

	public static boolean canWeAdd(Set<Label> set,AltLabel candidat){
		for (Label pl:set){
			if (pl.getLanguage().equals(candidat.getLanguage())){
				if (pl.getValue().equalsIgnoreCase(candidat.getValue())){
					return false;
				}
			}
		
		}
		return true;
	}
	
}
