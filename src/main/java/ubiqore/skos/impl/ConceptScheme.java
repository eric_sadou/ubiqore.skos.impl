/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;

import java.util.List;
import java.util.Set;

import org.openrdf.model.vocabulary.SKOS;

import ubiqore.skos.dc.Metadata;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ConceptScheme  extends Resource {
	private int previewTopConceptNumbers=0;
	private Metadata metadata;
	private String type=SKOS.CONCEPT_SCHEME.getLocalName();
	public String getType() {
		return type;
	}
	List<Concept> hasTopConcepts=Lists.newArrayList();
	Set<Concept> schemeOf=Sets.newHashSet();
	/**
	 * @return the hasTopConcepts
	 */
	public List<Concept> getHasTopConcepts() {
		return hasTopConcepts;
	}
	/**
	 * @param hasTopConcepts the hasTopConcepts to set
	 */
	public void setHasTopConcepts(List<Concept> hasTopConcepts) {
		this.hasTopConcepts = hasTopConcepts;
	}
	/**
	 * @return the schemeOf
	 */
	public Set<Concept> getSchemeOf() {
		return schemeOf;
	}
	/**
	 * @param schemeOf the schemeOf to set
	 */
	public void setSchemeOf(Set<Concept> schemeOf) {
		this.schemeOf = schemeOf;
	}
	public int getPreviewTopConceptNumbers() {
		return previewTopConceptNumbers;
	}
	public void setPreviewTopConceptNumbers(int previewTopConceptNumbers) {
		this.previewTopConceptNumbers = previewTopConceptNumbers;
	}
	public Metadata getMetadata() {
		return metadata;
	}
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}
	
}
