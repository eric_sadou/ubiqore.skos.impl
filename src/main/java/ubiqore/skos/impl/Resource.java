/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;

import java.util.Set;

import org.openrdf.model.URI;

import ubiqore.skos.impl.labels.AltLabel;
import ubiqore.skos.impl.labels.HiddenLabel;
import ubiqore.skos.impl.labels.PrefLabel;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;

public class Resource {
	protected URI uri;
	protected Set<Notation> notations=Sets.newHashSet();  // skos:notation
	protected Set<PrefLabel> prefLabels=Sets.newHashSet(); // skos:prefLabel
	protected Set<AltLabel> altLabels=Sets.newHashSet(); // skos:altLabel
	protected Set<HiddenLabel> hiddenLabels=Sets.newHashSet(); // skosk:hiddenLabel
	protected Set<Note> annotations=Sets.newHashSet();   // skos:note & subclass of note. 
	/**
	 * @return the uri
	 */
	@JsonSerialize(as=URI.class)
	public URI getUri() {
		return uri;
	}
	/**
	 * @param uri the uri to set
	 */
	public void setUri(URI uri) {
		this.uri = uri;
	}
	/**
	 * @return the notations
	 */
	public Set<Notation> getNotations() {
		return notations;
	}
	/**
	 * @param notations the notations to set
	 */
	public void setNotations(Set<Notation> notations) {
		this.notations = notations;
	}
	/**
	 * @return the prefLabels
	 */
	public Set<PrefLabel> getPrefLabels() {
		return prefLabels;
	}
	/**
	 * @param prefLabels the prefLabels to set
	 */
	public void setPrefLabels(Set<PrefLabel> prefLabels) {
		this.prefLabels = prefLabels;
	}
	/**
	 * @return the altLabels
	 */
	public Set<AltLabel> getAltLabels() {
		return altLabels;
	}
	/**
	 * @param altLabels the altLabels to set
	 */
	public void setAltLabels(Set<AltLabel> altLabels) {
		this.altLabels = altLabels;
	}
	/**
	 * @return the hiddenLabels
	 */
	public Set<HiddenLabel> getHiddenLabels() {
		return hiddenLabels;
	}
	/**
	 * @param hiddenLabels the hiddenLabels to set
	 */
	public void setHiddenLabels(Set<HiddenLabel> hiddenLabels) {
		this.hiddenLabels = hiddenLabels;
	}
	/**
	 * @return the annotations
	 */
	public Set<Note> getAnnotations() {
		return annotations;
	}
	/**
	 * @param annotations the annotations to set
	 */
	public void setAnnotations(Set<Note> annotations) {
		this.annotations = annotations;
	}
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("uri", uri)
				.add("notations",notations)
				.add("prefLabels",prefLabels)
				.add("altLabels", altLabels)
				.add("hiddenLabels", hiddenLabels)
				.add("notes",annotations)
				.toString();
	}
}
