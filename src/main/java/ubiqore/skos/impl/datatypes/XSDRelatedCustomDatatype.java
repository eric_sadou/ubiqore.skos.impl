/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl.datatypes;

import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.OWL;

public class XSDRelatedCustomDatatype {

	public String type=OWL.DATATYPEPROPERTY.getLocalName();
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public URI getUri() {
		return uri;
	}


	public void setUri(URI uri) {
		this.uri = uri;
	}


	

	public URI getDomain() {
		return domain;
	}


	public void setDomain(URI domain) {
		this.domain = domain;
	}


	public URI getXsdPrimitive() {
		return xsdPrimitive;
	}


	public void setXsdPrimitive(URI xsdPrimitive) {
		this.xsdPrimitive = xsdPrimitive;
	}


	public URI uri;
	
	public URI domain;
	public URI xsdPrimitive;
	
	
	public XSDRelatedCustomDatatype(URI uri,URI domain,URI xsdPrimitive ){
		this.uri=uri;
		this.domain=domain;
		this.xsdPrimitive=xsdPrimitive;
		
	}
	
}
