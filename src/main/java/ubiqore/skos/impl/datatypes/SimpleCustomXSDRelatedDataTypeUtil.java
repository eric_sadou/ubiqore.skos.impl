/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl.datatypes;

import java.util.List;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.model.vocabulary.SKOS;

import org.semarglproject.vocab.XSD;


import com.google.common.collect.Lists;

public class SimpleCustomXSDRelatedDataTypeUtil {
	/*
	 * DATATYpe for SKOS Notes & Skos Notations
	 * XSD  : XML schema datatypes 
	 * <owl:DatatypeProperty rdf:about="#creationDate">
	  <rdfs:domain rdf:resource=""/>
	  <rdf:range rdf:resource="&xsd;dateTime"/>
	</owl:DatatypeProperty>

	<Measurement>
	  <timeStamp rdf:datatype="&xsd;dateTime">2003-01-24T09:00:08+01:00</timeStamp>  
	</Measurement>*/
	
	/* 
	 * RDF Datatypes http://www.w3.org/TR/owl-ref/#rdf-datatype
	 * creation de la datatype : creationDate 
	 */
	public List<Statement> generateStatementForCustomDatatypeForSkos(URI newdatatypeUri,URI skosElement,URI primitiveXSD){
		List<Statement> l=Lists.newArrayList();
		try {if (newdatatypeUri.getLocalName()==null||newdatatypeUri.getLocalName().isEmpty())return null;}catch(Exception e){return null;}
		try {if (newdatatypeUri.getNamespace()==null||newdatatypeUri.getNamespace().isEmpty())return null;}catch(Exception e){return null;}
		if (!this.validateSkosElement(skosElement))return null;
		if (!this.validateXSDElement(primitiveXSD))return null;
		
		URI context=new URIImpl(newdatatypeUri.getNamespace());
		Statement st=new ContextStatementImpl(newdatatypeUri, RDF.TYPE,OWL.DATATYPEPROPERTY, context);
		Statement domain=new ContextStatementImpl(newdatatypeUri, RDFS.DOMAIN,skosElement, context);
		Statement range=new ContextStatementImpl(newdatatypeUri, RDFS.RANGE,primitiveXSD, context);
		
		l.add(st);l.add(domain);l.add(range);
		
		return l;
	}
	
	/*
	 * <owl:DatatypeProperty rdf:ID="tennisGameScore">
  <rdfs:range>
    <owl:DataRange>
      <owl:oneOf>
        <rdf:List>
           <rdf:first rdf:datatype="&xsd;integer">0</rdf:first>
           <rdf:rest>
             <rdf:List>
               <rdf:first rdf:datatype="&xsd;integer">15</rdf:first>
               <rdf:rest>
                 <rdf:List>
                   <rdf:first rdf:datatype="&xsd;integer">30</rdf:first>
                   <rdf:rest>
                     <rdf:List>
                       <rdf:first rdf:datatype="&xsd;integer">40</rdf:first>
                       <rdf:rest rdf:resource="&rdf;nil" />
                     </rdf:List>
                   </rdf:rest>
                 </rdf:List>
              </rdf:rest>
            </rdf:List>
          </rdf:rest>
        </rdf:List>
      </owl:oneOf>
    </owl:DataRange>
  </rdfs:range>
</owl:DatatypeProperty>
	 */
	
	
	
	public boolean validateSkosElement(URI uri){
		
		if (uri.equals(SKOS.NOTATION))return true;
		if (uri.equals(SKOS.NOTE))return true;
		if (uri.equals(SKOS.CHANGE_NOTE))return true;
		if (uri.equals(SKOS.DEFINITION))return true;
		if (uri.equals(SKOS.EDITORIAL_NOTE))return true;
		if (uri.equals(SKOS.EXAMPLE))return true;
		if (uri.equals(SKOS.HISTORY_NOTE))return true;
		if (uri.equals(SKOS.SCOPE_NOTE))return true;
		
		return false;
	}
	//  44 XSD declar??s  
	public boolean validateXSDElement(URI xsd){
		if (xsd.equals(new URIImpl(XSD.ANY_URI)))return true;
		if (xsd.equals(new URIImpl(XSD.BASE64_BINARY)))return true;
		if (xsd.equals(new URIImpl(XSD.BOOLEAN)))return true;
		if (xsd.equals(new URIImpl(XSD.BYTE)))return true;
		if (xsd.equals(new URIImpl(XSD.DATE)))return true;
		if (xsd.equals(new URIImpl(XSD.DATE_TIME)))return true;
		if (xsd.equals(new URIImpl(XSD.DECIMAL)))return true;
		if (xsd.equals(new URIImpl(XSD.DOUBLE)))return true;
		if (xsd.equals(new URIImpl(XSD.DURATION)))return true;
		if (xsd.equals(new URIImpl(XSD.ENTITIES)))return true;
		if (xsd.equals(new URIImpl(XSD.ENTITY)))return true;
		if (xsd.equals(new URIImpl(XSD.FLOAT)))return true;
		if (xsd.equals(new URIImpl(XSD.G_DAY)))return true;
		if (xsd.equals(new URIImpl(XSD.G_MONTH)))return true;
		if (xsd.equals(new URIImpl(XSD.G_MONTH_DAY)))return true;
		if (xsd.equals(new URIImpl(XSD.G_YEAR)))return true;
		if (xsd.equals(new URIImpl(XSD.G_YEAR_MONTH)))return true;
		if (xsd.equals(new URIImpl(XSD.HEX_BINARY)))return true;
		if (xsd.equals(new URIImpl(XSD.ID)))return true;
		if (xsd.equals(new URIImpl(XSD.IDREF)))return true;
		if (xsd.equals(new URIImpl(XSD.IDREFS)))return true;
		if (xsd.equals(new URIImpl(XSD.INT)))return true;
		if (xsd.equals(new URIImpl(XSD.INTEGER)))return true;
		if (xsd.equals(new URIImpl(XSD.LANGUAGE)))return true;
		if (xsd.equals(new URIImpl(XSD.LONG)))return true;
		if (xsd.equals(new URIImpl(XSD.NAME)))return true;
		if (xsd.equals(new URIImpl(XSD.NC_NAME)))return true;
		if (xsd.equals(new URIImpl(XSD.NEGATIVE_INTEGER)))return true;
		if (xsd.equals(new URIImpl(XSD.NMTOKEN)))return true;
		if (xsd.equals(new URIImpl(XSD.NMTOKENS)))return true;
		if (xsd.equals(new URIImpl(XSD.NON_NEGATIVE_INTEGER)))return true;
		if (xsd.equals(new URIImpl(XSD.NON_POSITIVE_INTEGER)))return true;
		if (xsd.equals(new URIImpl(XSD.NORMALIZED_STRING)))return true;
		if (xsd.equals(new URIImpl(XSD.NOTATION)))return true;
		if (xsd.equals(new URIImpl(XSD.POSITIVE_INTEGER)))return true;
		if (xsd.equals(new URIImpl(XSD.QNAME)))return true;
		if (xsd.equals(new URIImpl(XSD.SHORT)))return true;
		if (xsd.equals(new URIImpl(XSD.STRING)))return true;
		if (xsd.equals(new URIImpl(XSD.TIME)))return true;
		if (xsd.equals(new URIImpl(XSD.TOKEN)))return true;
		if (xsd.equals(new URIImpl(XSD.UNSIGNED_BYTE)))return true;
		if (xsd.equals(new URIImpl(XSD.UNSIGNED_INT)))return true;
		if (xsd.equals(new URIImpl(XSD.UNSIGNED_LONG)))return true;
		if (xsd.equals(new URIImpl(XSD.UNSIGNED_SHORT)))return true;
		
		return false;
	} 
	
	public static List<Statement> exampleDatatype(){
		List<Statement> l=Lists.newArrayList();
		String context="http://ubiqore.com/datatypes";
		Statement st=new ContextStatementImpl(new URIImpl(context+"#creationDate"), RDF.TYPE,OWL.DATATYPEPROPERTY, new URIImpl(context));
		Statement domain=new ContextStatementImpl(new URIImpl(context+"#creationDate"), RDFS.DOMAIN,SKOS.DEFINITION, new URIImpl(context));
		Statement range=new ContextStatementImpl(new URIImpl(context+"#creationDate"), RDFS.RANGE,new URIImpl(XSD.DATE_TIME), new URIImpl(context));
		l.add(st);l.add(domain);l.add(range);
		return l;
	}
}
