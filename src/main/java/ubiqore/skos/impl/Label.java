/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;


import org.openrdf.model.Literal;
import org.openrdf.model.impl.LiteralImpl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import ubiqore.skos.impl.lang.Lang;

public class Label implements StringInfo{
	
	String value;
	
	String language="en";
	
	public Label(){
		
	}
	public Label(Literal lit){
		this.value=lit.getLabel();
		String tmp=lit.getLanguage();
		try {
			if (Lang.CHECK(tmp))this.language=tmp;
		}
		catch(Exception e ){
			this.language="en";
		}
	
	}
	public Label(String st){
		this.value=st;
	}

	public Label(String st,String language){
		this.value=st;
		try {
			if (Lang.CHECK(language))this.language=language;
		}
		catch(Exception e ){
			this.language="en";
		}
	}
	
	@JsonIgnore
	public Literal getLit() {
		return new LiteralImpl(value,language);
	}

	

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("value", value)
				.add("language", language)
				.toString();
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		try {
			if (Lang.CHECK(language))this.language=language;
		}
		catch(Exception e ){
			
		}
	}
		

}
