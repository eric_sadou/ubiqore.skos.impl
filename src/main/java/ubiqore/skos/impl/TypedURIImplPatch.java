/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;

import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.URIImpl;

public class TypedURIImplPatch implements Value {
	/**
	 * 
	 */
	public TypedURIImplPatch(String uri,URI datatype){
		System.out.println("hehe");
		this.uri=new URIImpl(uri);
		this.datatype=datatype;
	} 
	
	public TypedURIImplPatch(URI uri,URI datatype){
		this.uri=uri;
		this.datatype=datatype;
	}
	private static final long serialVersionUID = 1L;
	URI uri;
	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public URI getDatatype() {
		return datatype;
	}

	public void setDatatype(URI datatype) {
		this.datatype = datatype;
	}

	URI datatype;
	
	public String stringValue() {
		// TODO Auto-generated method stub
		return this.getUri().toString()+":withDataType:"+this.datatype.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(uri.toString().length() * 2);

		sb.append('"');
		sb.append(uri.toString());
		sb.append('"');

		

		if (datatype != null) {
			sb.append("^^<");
			sb.append(datatype.toString());
			sb.append(">");
		}

		return sb.toString();
	}
}
