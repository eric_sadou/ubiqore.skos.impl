/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;

import java.util.List;
import java.util.Set;

import org.openrdf.model.vocabulary.SKOS;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class OrderedCollection extends Collection {

	List<CollectionEntity> memberLists=Lists.newArrayList();
	
	
	
	public List<CollectionEntity> getMemberLists() {
		return memberLists;
	}
	
	@Override
	public Set<CollectionEntity> getMembers(){
		
		return Sets.newHashSet(memberLists); 
	}

	public String getType() {
		return SKOS.ORDERED_COLLECTION.getLocalName();
	}
	
	public void setMemberLists(List<CollectionEntity> memberLists) {
		this.members=Sets.newHashSet();
		for (CollectionEntity e:memberLists){
			this.getMembers().add(e);
		}
		this.memberLists = memberLists;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("uri", this.getUri())
				.add("notations",notations)
				.add("prefLabels",prefLabels)
				.add("altLabels", altLabels)
				.add("hiddenLabels", hiddenLabels)
				.add("notes",annotations)
				.add("memberLists", memberLists)
				.toString();
	}
	
}
