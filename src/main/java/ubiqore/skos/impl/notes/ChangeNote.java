/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl.notes;


import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.SKOS;

import ubiqore.skos.impl.Note;

public class ChangeNote extends Note {
	
	
	public ChangeNote(String value, String langage) {
		super(value, langage);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());
	}
	/*public ChangeNote(String value, URI datatype) {
		super(value, datatype);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());	}*/
	public ChangeNote (URI uri){
		super(uri);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());
	}
	
	
	
	public ChangeNote(Literal objLit) {
		super(objLit);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());
	}
	
	public ChangeNote(Literal objLit,URI context) {
		super(objLit,context);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());
	}
	
	public ChangeNote(URI uri,URI context) {
		super(uri,context);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());	}
	
	public ChangeNote(String value, URI datatype,URI context) {
		super(value, datatype,context);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());	}
	
	public ChangeNote(String value, String langue,URI context) {
		super(value, langue,context);
		this.setType(SKOS.CHANGE_NOTE.getLocalName());	}
}
