/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.impl;

import java.util.Set;

import org.openrdf.model.URI;
import org.openrdf.model.vocabulary.SKOS;

import ubiqore.skos.impl.labels.AltLabel;
import ubiqore.skos.impl.labels.HiddenLabel;
import ubiqore.skos.impl.labels.PrefLabel;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;

public class Collection extends Resource implements  CollectionEntity,StringInfo{
	
	Set<URI> memberOf=Sets.newHashSet(); // implementation of collectionEntity 
	
	Set<CollectionEntity> members=Sets.newHashSet();
	
	long size=0;
	
	protected String type=SKOS.COLLECTION.getLocalName();
	
	
	public Set<URI> getMemberOf(){
		return memberOf;
	}
	public Set<CollectionEntity> getMembers() {
		return members;
	}
	
	public long getSize() {
		return size;
	}
	
	public String getType() {
		return type;
	}
	public void setMemberOf(Set<URI> set){
		this.memberOf=set;
	}

	public void setMembers(Set<CollectionEntity> members) {
		this.members = members;
	}

	public void setSize(long size) {
		this.size = size;
	}

	
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this.getClass()).add("uri", uri)
				.add("notations",notations)
				.add("prefLabels",prefLabels)
				.add("altLabels", altLabels)
				.add("hiddenLabels", hiddenLabels)
				.add("notes",annotations)
				.add("members", members)
				.toString();
	}
	
}
