/*
 * #%L
 * ubiqore.skos.impl
 * %%
 * Copyright (C) 2014 - 2015 Eric Sadou
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package ubiqore.skos.dc;

import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Value;

import com.google.common.collect.Sets;

// dcterms
public class Metadata {

	Set<Value> abstract2 = Sets.newHashSet(), 
			      accessRights = Sets.newHashSet(), 
			      accrualMethod = Sets.newHashSet(),
			accrualPeriodicity = Sets.newHashSet(), 
			accrualPolicy = Sets.newHashSet(),
			alternative = Sets.newHashSet(),
			audience = Sets.newHashSet(),
			available = Sets.newHashSet(),
			bibliographicCitation = Sets.newHashSet(),
			conformsTo = Sets.newHashSet(), 
			contributor = Sets.newHashSet(),
			coverage = Sets.newHashSet(), 
			creator = Sets.newHashSet(),
			description = Sets.newHashSet(),
			educationLevel = Sets.newHashSet(),
			extent = Sets.newHashSet(),
			format = Sets.newHashSet(), 
			hasFormat = Sets.newHashSet(),
			hasPart = Sets.newHashSet(),
			hasVersion = Sets.newHashSet(),
			identifier = Sets.newHashSet(),
			instructionalMethod = Sets.newHashSet(),
			isFormatOf = Sets.newHashSet(),
			isPartOf = Sets.newHashSet(),
			isReferencedBy = Sets.newHashSet(),
			isReplacedBy = Sets.newHashSet(),
			isRequiredBy = Sets.newHashSet(),
			isVersionOf = Sets.newHashSet(), license = Sets.newHashSet(),
			mediator = Sets.newHashSet(), medium = Sets.newHashSet(),
			provenance = Sets.newHashSet(), publisher = Sets.newHashSet(),
			references = Sets.newHashSet(), relation = Sets.newHashSet(),
			replaces = Sets.newHashSet(), requires = Sets.newHashSet(),
			rights = Sets.newHashSet(), rightsHolder = Sets.newHashSet(),
			source = Sets.newHashSet(), spatial = Sets.newHashSet(),
			subject = Sets.newHashSet(), tableOfContents = Sets.newHashSet(),
			temporal = Sets.newHashSet(), type = Sets.newHashSet();

	Set<Literal> created = Sets.newHashSet(), date = Sets.newHashSet(),
			dateAccepted = Sets.newHashSet(), dateCopyrighted = Sets
					.newHashSet(), dateSubmitted = Sets.newHashSet(),
			issued = Sets.newHashSet(), language = Sets.newHashSet(),
			modified = Sets.newHashSet(), title = Sets.newHashSet(),
			valid = Sets.newHashSet();

	public Set<Value> getAbstract2() {
		return abstract2;
	}

	public void setAbstract2(Set<Value> abstract2) {
		this.abstract2 = abstract2;
	}

	public Set<Value> getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(Set<Value> accessRights) {
		this.accessRights = accessRights;
	}

	public Set<Value> getAccrualMethod() {
		return accrualMethod;
	}

	public void setAccrualMethod(Set<Value> accrualMethod) {
		this.accrualMethod = accrualMethod;
	}

	public Set<Value> getAccrualPeriodicity() {
		return accrualPeriodicity;
	}

	public void setAccrualPeriodicity(Set<Value> accrualPeriodicity) {
		this.accrualPeriodicity = accrualPeriodicity;
	}

	public Set<Value> getAccrualPolicy() {
		return accrualPolicy;
	}

	public void setAccrualPolicy(Set<Value> accrualPolicy) {
		this.accrualPolicy = accrualPolicy;
	}

	public Set<Value> getAlternative() {
		return alternative;
	}

	public void setAlternative(Set<Value> alternative) {
		this.alternative = alternative;
	}

	public Set<Value> getAudience() {
		return audience;
	}

	public void setAudience(Set<Value> audience) {
		this.audience = audience;
	}

	public Set<Value> getAvailable() {
		return available;
	}

	public void setAvailable(Set<Value> available) {
		this.available = available;
	}

	public Set<Value> getBibliographicCitation() {
		return bibliographicCitation;
	}

	public void setBibliographicCitation(Set<Value> bibliographicCitation) {
		this.bibliographicCitation = bibliographicCitation;
	}

	public Set<Value> getConformsTo() {
		return conformsTo;
	}

	public void setConformsTo(Set<Value> conformsTo) {
		this.conformsTo = conformsTo;
	}

	public Set<Value> getContributor() {
		return contributor;
	}

	public void setContributor(Set<Value> contributor) {
		this.contributor = contributor;
	}

	public Set<Value> getCoverage() {
		return coverage;
	}

	public void setCoverage(Set<Value> coverage) {
		this.coverage = coverage;
	}

	public Set<Value> getCreator() {
		return creator;
	}

	public void setCreator(Set<Value> creator) {
		this.creator = creator;
	}

	public Set<Value> getDescription() {
		return description;
	}

	public void setDescription(Set<Value> description) {
		this.description = description;
	}

	public Set<Value> getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(Set<Value> educationLevel) {
		this.educationLevel = educationLevel;
	}

	public Set<Value> getExtent() {
		return extent;
	}

	public void setExtent(Set<Value> extent) {
		this.extent = extent;
	}

	public Set<Value> getFormat() {
		return format;
	}

	public void setFormat(Set<Value> format) {
		this.format = format;
	}

	public Set<Value> getHasFormat() {
		return hasFormat;
	}

	public void setHasFormat(Set<Value> hasFormat) {
		this.hasFormat = hasFormat;
	}

	public Set<Value> getHasPart() {
		return hasPart;
	}

	public void setHasPart(Set<Value> hasPart) {
		this.hasPart = hasPart;
	}

	public Set<Value> getHasVersion() {
		return hasVersion;
	}

	public void setHasVersion(Set<Value> hasVersion) {
		this.hasVersion = hasVersion;
	}

	public Set<Value> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Set<Value> identifier) {
		this.identifier = identifier;
	}

	public Set<Value> getInstructionalMethod() {
		return instructionalMethod;
	}

	public void setInstructionalMethod(Set<Value> instructionalMethod) {
		this.instructionalMethod = instructionalMethod;
	}

	public Set<Value> getIsFormatOf() {
		return isFormatOf;
	}

	public void setIsFormatOf(Set<Value> isFormatOf) {
		this.isFormatOf = isFormatOf;
	}

	public Set<Value> getIsPartOf() {
		return isPartOf;
	}

	public void setIsPartOf(Set<Value> isPartOf) {
		this.isPartOf = isPartOf;
	}

	public Set<Value> getIsReferencedBy() {
		return isReferencedBy;
	}

	public void setIsReferencedBy(Set<Value> isReferencedBy) {
		this.isReferencedBy = isReferencedBy;
	}

	public Set<Value> getIsReplacedBy() {
		return isReplacedBy;
	}

	public void setIsReplacedBy(Set<Value> isReplacedBy) {
		this.isReplacedBy = isReplacedBy;
	}

	public Set<Value> getIsRequiredBy() {
		return isRequiredBy;
	}

	public void setIsRequiredBy(Set<Value> isRequiredBy) {
		this.isRequiredBy = isRequiredBy;
	}

	public Set<Value> getIsVersionOf() {
		return isVersionOf;
	}

	public void setIsVersionOf(Set<Value> isVersionOf) {
		this.isVersionOf = isVersionOf;
	}

	public Set<Value> getLicense() {
		return license;
	}

	public void setLicense(Set<Value> license) {
		this.license = license;
	}

	public Set<Value> getMediator() {
		return mediator;
	}

	public void setMediator(Set<Value> mediator) {
		this.mediator = mediator;
	}

	public Set<Value> getMedium() {
		return medium;
	}

	public void setMedium(Set<Value> medium) {
		this.medium = medium;
	}

	public Set<Value> getProvenance() {
		return provenance;
	}

	public void setProvenance(Set<Value> provenance) {
		this.provenance = provenance;
	}

	public Set<Value> getPublisher() {
		return publisher;
	}

	public void setPublisher(Set<Value> publisher) {
		this.publisher = publisher;
	}

	public Set<Value> getReferences() {
		return references;
	}

	public void setReferences(Set<Value> references) {
		this.references = references;
	}

	public Set<Value> getRelation() {
		return relation;
	}

	public void setRelation(Set<Value> relation) {
		this.relation = relation;
	}

	public Set<Value> getReplaces() {
		return replaces;
	}

	public void setReplaces(Set<Value> replaces) {
		this.replaces = replaces;
	}

	public Set<Value> getRequires() {
		return requires;
	}

	public void setRequires(Set<Value> requires) {
		this.requires = requires;
	}

	public Set<Value> getRights() {
		return rights;
	}

	public void setRights(Set<Value> rights) {
		this.rights = rights;
	}

	public Set<Value> getRightsHolder() {
		return rightsHolder;
	}

	public void setRightsHolder(Set<Value> rightsHolder) {
		this.rightsHolder = rightsHolder;
	}

	public Set<Value> getSource() {
		return source;
	}

	public void setSource(Set<Value> source) {
		this.source = source;
	}

	public Set<Value> getSpatial() {
		return spatial;
	}

	public void setSpatial(Set<Value> spatial) {
		this.spatial = spatial;
	}

	public Set<Value> getSubject() {
		return subject;
	}

	public void setSubject(Set<Value> subject) {
		this.subject = subject;
	}

	public Set<Value> getTableOfContents() {
		return tableOfContents;
	}

	public void setTableOfContents(Set<Value> tableOfContents) {
		this.tableOfContents = tableOfContents;
	}

	public Set<Value> getTemporal() {
		return temporal;
	}

	public void setTemporal(Set<Value> temporal) {
		this.temporal = temporal;
	}

	public Set<Value> getType() {
		return type;
	}

	public void setType(Set<Value> type) {
		this.type = type;
	}

	public Set<Literal> getCreated() {
		return created;
	}

	public void setCreated(Set<Literal> created) {
		this.created = created;
	}

	public Set<Literal> getDate() {
		return date;
	}

	public void setDate(Set<Literal> date) {
		this.date = date;
	}

	public Set<Literal> getDateAccepted() {
		return dateAccepted;
	}

	public void setDateAccepted(Set<Literal> dateAccepted) {
		this.dateAccepted = dateAccepted;
	}

	public Set<Literal> getDateCopyrighted() {
		return dateCopyrighted;
	}

	public void setDateCopyrighted(Set<Literal> dateCopyrighted) {
		this.dateCopyrighted = dateCopyrighted;
	}

	public Set<Literal> getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(Set<Literal> dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public Set<Literal> getIssued() {
		return issued;
	}

	public void setIssued(Set<Literal> issued) {
		this.issued = issued;
	}

	public Set<Literal> getLanguage() {
		return language;
	}

	public void setLanguage(Set<Literal> language) {
		this.language = language;
	}

	public Set<Literal> getModified() {
		return modified;
	}

	public void setModified(Set<Literal> modified) {
		this.modified = modified;
	}

	public Set<Literal> getTitle() {
		return title;
	}

	public void setTitle(Set<Literal> title) {
		this.title = title;
	}

	public Set<Literal> getValid() {
		return valid;
	}

	public void setValid(Set<Literal> valid) {
		this.valid = valid;
	}

}
